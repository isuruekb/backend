import { Module } from '@nestjs/common';
import { UserSessionController } from './user-session.controller';
import { UserSessionService } from './user-session.service';

@Module({
  controllers: [UserSessionController],
  providers: [UserSessionService],
})
export class UserSessionModule {}
