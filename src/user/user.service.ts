import { Injectable } from '@nestjs/common';
import { Response } from 'express';
import { AuthService } from 'src/auth/auth.service';
import { userRegistrationDto } from 'src/shared/dto/userRegistration.dto';
import * as bcrypt from 'bcrypt';
import { User } from 'src/database/entities/user.entity';

@Injectable()
export class UserService {
  constructor(private authService: AuthService) {}

  public async register(res: Response, body: userRegistrationDto) {
    try {
      if (!(await this.isEmailExists(body.email))) {
        const salt = await bcrypt.genSalt();
        const user: User = new User();

        user.email = body.email;
        user.userType = body.userType;
        user.password = await bcrypt.hash(body.password, salt);

        await user.save();

        this.authService.login(res, {
          email: user.email,
          password: body.password,
        });
      } else {
        this.sendDuplicateResponse(res);
      }
    } catch (error: any) {
      this.sendDuplicateResponse(res);
    }
  }

  private async isEmailExists(email: string): Promise<boolean> {
    const user: User = await User.findOne({ email: email });
    return user !== undefined;
  }

  private sendDuplicateResponse(res: Response): Response {
    return res.status(400).json({
      status: 'FAILED',
      message: 'User already exisits',
    });
  }

  async findAllUsers(res: Response, limit?: number, offset?: number) {
    const users: User[] = await User.find({
      take: limit ? limit : 20,
      skip: offset ? offset : 0,
      where: {
        status: true,
      },
    });
    return res.status(200).json({
      status: 'SUCCESS',
      result: users,
    });
  }
}
