import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { userRegistrationDto } from '../shared/dto/userRegistration.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('register')
  public registerAdmin(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: userRegistrationDto,
  ) {
    this.userService.register(res, body);
  }

  @Get('getAllUser')
  public findAllUsers(@Req() req: Request, @Res() res: Response) {
    this.userService.findAllUsers(res);
  }
}
