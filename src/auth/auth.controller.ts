import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthService } from './auth.service';
import { LoginDto } from '../shared/dto/login.dto';
import { JwtAuthGuard } from './guards/jws-auth.guards';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  login(@Req() req: Request, @Res() res: Response, @Body() body: LoginDto) {
    this.authService.login(res, body);
  }

  @Post('logout')
  logout(@Req() req: Request, @Res() res: Response) {
    return res.status(200).end();
  }

  @UseGuards(JwtAuthGuard)
  @Get('whoIsThis')
  whoIsThis(@Req() req: Request, @Res() res: Response) {
    this.authService.whoIsThis(req, res);
  }
}
