import { HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/database/entities/user.entity';
import * as bcrypt from 'bcrypt';
import { LoginDto, LoginResponseDto } from '../shared/dto/login.dto';
import { Request, Response } from 'express';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) { }

  private async userValidation(email: string, password: string) {
    const user: User = await User.findOne({ email: email });
    if (user && (await bcrypt.compare(password, user.password))) {
      return user;
    }
    return null;
  }

  async login(res: Response, body: LoginDto) {
    const user: User = await this.userValidation(body.email, body.password);

    if (!user) {
      return res.status(HttpStatus.BAD_REQUEST).send('Invalid Credentials');
    }

    if (!user.status) {
      return res
        .status(HttpStatus.UNAUTHORIZED)
        .send('Unautharized User Login');
    }

    if (user) {
      const payload = {
        user: {
          id: user.id,
          email: user.email,
          user_type: user.userType,
        },
      };
      return res
        .status(HttpStatus.OK)
        .json(new LoginResponseDto(this.jwtService.sign(payload)));
    }
  }

  async whoIsThis(req: Request, res: Response) {
    const payload: any = req.user; //how its takes user from request
    if (!payload) {
      return res.status(HttpStatus.UNAUTHORIZED).send('You are not Authorized');
    }

    const user: User = await User.findOne({ id: payload.id });
    if (user) {
      return res.status(HttpStatus.OK).json({
          ...user, //how its works and whats it get
          password: null,
          picture: 'assets/img/avatar.jpg',
          // must add picture url if you want
          name: `${user.fName} ${user.lName}`,
        });
    } else {
      return res.status(HttpStatus.UNAUTHORIZED).end();
    }
  }
}
