import { User } from "src/database/entities/user.entity";

export class CreateRequestDto {
    
    id: number;
    title: string;
    status: boolean;
    date: string;
    time: string;
    message: string;
    imageID: string;
    user: User;
}