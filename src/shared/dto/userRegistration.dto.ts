import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { UserType } from 'src/database/enums/userType.enum';
import { BaseDto } from 'src/shared/dto/base.dto';

export class userRegistrationDto extends BaseDto {
  
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;
  
  @IsNotEmpty()
  @IsString()
  userType: UserType;
}
