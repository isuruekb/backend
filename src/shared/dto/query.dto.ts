export class QueryDto {
    public limit?: number;
    public offset?: number;
    public order?: 'ASC' | 'DESC';
}