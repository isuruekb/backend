import { IsNumber, IsOptional } from 'class-validator';

export class BaseDto {
  @IsOptional()
  @IsNumber()
  public id: number;
}
