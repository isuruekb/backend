import { Body, Controller, Delete, Get, Param, Post, Res } from '@nestjs/common';
import { QueryDto } from 'src/shared/dto/query.dto';
import { CreateRequestDto } from 'src/shared/dto/request.dto';
import { RequestService } from './request.service';
import { Response } from 'express';

@Controller('request')
export class RequestController {
    constructor(private requestServise: RequestService) {}

    @Post('create')
    create(@Res() res: Response, @Body() body: CreateRequestDto): void {
        this.requestServise.create(res, body);
    }

    @Get('find/:id')
    findOne(@Res() res: Response, @Param('id') id) {
        this.requestServise.find(res, id);
    }

    @Post('query')
    findAll(@Res() res: Response, @Body() body: QueryDto) {
        return this.requestServise.query(res, body);
    }

    @Delete('delete/:id')
    delete(@Res() res: Response, @Param('id') id) {
        this.requestServise.remove(res, id);
    }
}
