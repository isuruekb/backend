import { Injectable, Req } from '@nestjs/common';
import { CreateRequestDto } from 'src/shared/dto/request.dto';
import { Response } from 'express';
import { Request } from 'src/database/entities/request.entity';
import { QueryDto } from 'src/shared/dto/query.dto';

@Injectable()
export class RequestService {

    constructor() {

    }

    public async create(res: Response, requestdto: CreateRequestDto) {
        const model: Request = requestdto.id ? await Request.findOne({ id: requestdto.id}) : new Request();
        model.title = requestdto.title;
        model.message = requestdto.message;
    
        await model.save();
        return res.status(200).json(model);
    }

    public async find(res: Response, id: number) {
        const model: Request = await Request.findOne({ id: id });
        return res.status(200).json(model);
    }

    public async query(res: Response, query: QueryDto) {
        const results: Request[] = await Request.find({
          take: query.limit ? query.limit : 10,
          skip: query.offset ? query.offset : 0,
          order: {
            id: query.order ? query.order : 'ASC'
          }
        });
        return res.status(200).json(results);
      }

      public async remove(res: Response, id: number) {
        await (await Request.findOne({ id: id })).remove();
        return res.status(200).send();
      }
    
}
