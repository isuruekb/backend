import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Donation } from './database/entities/donation.entity';
import { User } from './database/entities/user.entity';
import { UserSession } from './database/entities/userSession.entity';
import { UserSessionModule } from './user-session/user-session.module';
import { UserModule } from './user/user.module';
import { DonationModule } from './donation/donation.module';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { Chat } from './database/entities/chat.entity';
import { Feedback } from './database/entities/feedback.entity';
import { Notification } from './database/entities/notification.entity';
import { Request } from './database/entities/request.entity';
import { Photo } from './database/entities/photo.entity';
import { RequestModule } from './request/request.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'smsdb',
      entities: [User, UserSession, Chat, Donation, Feedback, Notification, Request, Photo],
      synchronize: true,
    }),
    UserModule,
    UserSessionModule,
    DonationModule,
    AuthModule,
    SharedModule,
    RequestModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
