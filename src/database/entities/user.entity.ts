import { Column, Entity, OneToMany } from 'typeorm';
import { UserType } from '../enums/userType.enum';
import { Base } from './base.entity';
import { Chat } from './chat.entity';
import { Donation } from './donation.entity';
import { Feedback } from './feedback.entity';
import { Notification } from './notification.entity';
import { Photo } from './photo.entity';
import { Request } from './request.entity';
import { UserSession } from './userSession.entity';

@Entity()
export class User extends Base {
  // @PrimaryGeneratedColumn()
  // userID: number;

  @Column({ unique: true })
  email: string;

  @Column({ nullable: true })
  fName: string;

  @Column({ nullable: true })
  lName: string;

  @Column({ nullable: true })
  password: string;

  @Column({ nullable: true })
  org: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  contact: string;

  @Column()
  userType: UserType;

  @Column({ default: true })
  status: boolean;

  @OneToMany(() => Donation, donation => donation.user)
  donation: Donation;

  @OneToMany(() => Chat, chat => chat.user)
  chat: Chat;

  @OneToMany(() => Feedback, feedback => feedback.user)
  feedback: Feedback;

  @OneToMany(() => Notification, notification => notification.user)
  notification: Notification;

  @OneToMany(() => UserSession, usersession => usersession.user)
  usersession: UserSession;

  @OneToMany(() => Request, request => request.user)
  request: Request;

  @OneToMany(() => Photo, photo => photo.user)
  photo: Photo;
}
