import { BaseEntity, PrimaryGeneratedColumn } from 'typeorm';

export class Base extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;
}
