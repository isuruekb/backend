import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Notification {

    @PrimaryGeneratedColumn()
    notificationID: number;

    @Column({ nullable: true })
    type: string;

    @Column({ nullable: true })
    message: string;

    @ManyToOne(() => User, user => user.feedback)
    user: User;
}