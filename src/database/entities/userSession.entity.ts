import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Base } from './base.entity';
import { User } from './user.entity';

@Entity()
export class UserSession {
  
  @PrimaryGeneratedColumn()
  idSessoion: number;

  @Column({ nullable: true })
  loginTime: number;

  @Column({ nullable: true })
  logoutTime: string;

  @ManyToOne(() => User, user => user.usersession)
  user: User;
}
