import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Base } from './base.entity';
import { User } from './user.entity';

@Entity()
export class Donation {

  @PrimaryGeneratedColumn()
  donationID: number;
  
  @Column({ nullable: true })
  title: string;

  @Column({ nullable: true })
  date: string;

  @Column({ nullable: true })
  time: string;

  @Column({ nullable: true })
  expireDate: string;

  @Column({ nullable: true })
  details: string;

  @Column({ default: true })
  type: boolean;

  @ManyToOne(() => User, user => user.donation)
  user: User;
}
