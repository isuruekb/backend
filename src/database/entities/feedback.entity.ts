import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Base } from "./base.entity";
import { User } from "./user.entity";

@Entity()
export class Feedback {
   1
    @PrimaryGeneratedColumn()
    feedbackID: number;

    @Column({ nullable: true })
    comment: string;

    @Column({ nullable: true })
    time: string;

    @Column({ nullable: true })
    date: string;

    @ManyToOne(() => User, user => user.feedback)
    user: User;

}
