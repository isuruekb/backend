import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Photo {

    @PrimaryGeneratedColumn()
    photoID: string;

    @Column()
    link: string;

    @ManyToOne(() => User, user => user.photo)
    user: User;
}