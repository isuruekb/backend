import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Base } from "./base.entity";
import { User } from "./user.entity";

@Entity()
export class Chat {

    @PrimaryGeneratedColumn()
    messageID: number;
    
    @Column({ default: true })
    status: boolean;

    @Column({ nullable: true })
    time: string;

    @Column({ nullable: true })
    reciverID: string;

    @Column({ nullable: true })
    message: string;

    @Column({ nullable: true })
    date: string;

    @ManyToOne(() => User, user => user.chat)
    user: User;

}