import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Request extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    title: string;

    @Column({ default: true })
    status: boolean;

    @Column({ nullable: true })
    date: string;

    @Column({ nullable: true })
    time: string;

    @Column({ nullable: true })
    message: string;

    @Column({ nullable: true })
    imageID: string;

    @ManyToOne(() => User, user => user.request)
    user: User;
}
